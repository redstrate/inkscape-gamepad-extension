#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2023 Joshua Goins, josh@redstrate.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
This plugin is used to generate svg files for buttons and qml file for the gamepad
KDE Control Module.
"""

import inkex
import copy
import os
import subprocess

class GamepadExportExtension(inkex.EffectExtension):
    """Exports an svg of a gamepad to separate svg files and a qml file"""
    def add_arguments(self, pars):
        pars.add_argument("--path", type=str, help="An example option, put your options here")
        pars.add_argument("--prefix", type=str, help="Prefix for the export, e.g. xbox, sony, valve, etc.")

    def effect(self):
        qml = ""

        filepath = self.options.input_file

        prefix = self.options.prefix

        if prefix == "":
            # Give it a default
            prefix = "xbox"

        path = self.options.path

        if path == "":
            # Another typical default
            path = "~/kde/src/plasma-desktop/kcms/gamepad/package/contents/ui/"

        path = os.path.expanduser(path)

        # Make sure svg path exists
        svgpath = os.path.join(path, "svg")
        os.makedirs(svgpath, exist_ok=True)

        button_mapping = {
            "D-pad Left": "SDL_CONTROLLER_BUTTON_DPAD_LEFT",
            "D-pad Right": "SDL_CONTROLLER_BUTTON_DPAD_RIGHT",
            "D-pad Up": "SDL_CONTROLLER_BUTTON_DPAD_UP",
            "D-pad Down": "SDL_CONTROLLER_BUTTON_DPAD_DOWN",
            "Start Button": "SDL_CONTROLLER_BUTTON_START",
            "Back Button": "SDL_CONTROLLER_BUTTON_BACK",
            "Guide Button": "SDL_CONTROLLER_BUTTON_GUIDE",
            "Misc Button": "SDL_CONTROLLER_BUTTON_MISC1",
            "B Button": "SDL_CONTROLLER_BUTTON_B",
            "Y Button": "SDL_CONTROLLER_BUTTON_Y",
            "X Button": "SDL_CONTROLLER_BUTTON_X",
            "A Button": "SDL_CONTROLLER_BUTTON_A",
            "Left Shoulder Button": "SDL_CONTROLLER_BUTTON_LEFTSHOULDER",
            "Right Shoulder Button": "SDL_CONTROLLER_BUTTON_RIGHTSHOULDER",
        }

        stick_mapping = {
            "Left Axis": { "axis": "SDL_CONTROLLER_AXIS_LEFTX", "button": "SDL_CONTROLLER_BUTTON_LEFTSTICK"},
            "Right Axis": { "axis": "SDL_CONTROLLER_AXIS_RIGHTX", "button": "SDL_CONTROLLER_BUTTON_RIGHTSTICK"},
        }

        trigger_mapping = {
            "Left Trigger Button": "SDL_CONTROLLER_AXIS_TRIGGERLEFT",
            "Right Trigger Button": "SDL_CONTROLLER_AXIS_TRIGGERRIGHT",
        }

        width = self.svg.viewbox_width
        height = self.svg.viewbox_height

        qml += "import QtQuick 2.15\n"
        qml += "import QtQuick.Window 2.2\n"
        qml += "import QtQuick.Layouts 1.1\n"
        qml += "import org.kde.kirigami 2.13 as Kirigami\n"
        qml += "import org.kde.kcmutils as KCM\n"
        qml += "import QtQuick.Controls 2.0 as QQC2\n"
        qml += "import QtQuick.Layouts 1.3 as Layouts\n"
        qml += "import org.kde.plasma.gamepad.kcm 1.0\n"
        qml += "import QtQuick.Shapes 1.15\n"
        qml += "import Qt5Compat.GraphicalEffects\n"
        qml += "import \"..\"\n"

        qml += "Item {\n"
        qml += "    id: root\n"
        qml += "    required property var device\n"
        qml += "    readonly property real relativeWidth: image.paintedWidth\n"
        qml += "    readonly property real relativeHeight: image.paintedHeight\n"
        qml += "    implicitWidth: image.paintedWidth\n"
        qml += "\n"

        qml += "    Image {\n"
        qml += "        id: image\n"
        qml += f"        source: \"../svg/{prefix}_Controller.svg\"\n"
        qml += "        fillMode: Image.PreserveAspectFit\n"
        qml += "        height: parent.height\n"
        qml += "        sourceSize.width: paintedWidth\n"
        qml += "        sourceSize.height: paintedHeight\n"
        qml += "    }\n"
        qml += "\n"

        qml += "    ColorOverlay {\n"
        qml += "        anchors.fill: image\n"
        qml += "        source: image\n"
        qml += "        color: Kirigami.Theme.disabledTextColor\n"
        qml += "    }\n"
        qml += "\n"

        for layer in self.svg:
            filename = os.path.join(svgpath, f"{prefix}_{layer.label}.svg")
            layerid = layer.attrib["id"]

            if layer.label and layer.label in button_mapping:
                posX = layer.bounding_box().left / width
                posY = layer.bounding_box().top / height
                box = layer.shape_box()
                itemWidth = box.width
                itemHeight = box.height

                sizeWidth = layer.bounding_box().width / width
                sizeHeight = layer.bounding_box().height / height

                qml += "    GamepadButton {\n"
                qml += f"        idx: GamepadButton.{button_mapping[layer.label]}\n"
                qml += "        device: root.device\n"
                qml += f"        image: \"./svg/{prefix}_{layer.label}.svg\"\n"
                qml += f"        posX: {posX}\n"
                qml += f"        posY: {posY}\n"
                qml += f"        sizeWidth: {sizeWidth}\n"
                qml += f"        sizeHeight: {sizeHeight}\n"
                qml += "        controllerWidth: root.relativeWidth\n"
                qml += "        controllerHeight: root.relativeHeight\n"
                qml += "    }\n\n"

                self.export_svg_file(layerid, filename, filepath)

            elif layer.label and layer.label in trigger_mapping:
                posX = layer.bounding_box().left / width
                posY = layer.bounding_box().top / height
                sizeWidth = layer.bounding_box().width / width
                sizeHeight = layer.bounding_box().height / height

                qml += "    GamepadTrigger {\n"
                qml += f"        idx: GamepadButton.{trigger_mapping[layer.label]}\n"
                qml += "        device: root.device\n"
                qml += f"        posX: {posX}\n"
                qml += f"        posY: {posY}\n"
                qml += f"        sizeWidth: {sizeWidth}\n"
                qml += f"        sizeHeight: {sizeHeight}\n"
                qml += "        controllerWidth: root.relativeWidth\n"
                qml += "        controllerHeight: root.relativeHeight\n"
                qml += f"        image: \"./svg/{prefix}_{layer.label}.svg\"\n"
                qml += "    }\n\n"

                self.export_svg_file(layerid, filename, filepath)

            elif layer.label and layer.label in stick_mapping:
                posX = layer.bounding_box().left / width
                posY = layer.bounding_box().top / height
                sizeWidth = layer.bounding_box().width / width
                sizeHeight = layer.bounding_box().height / height
                index = list(stick_mapping.keys()).index(layer.label)

                qml += "    GamepadStick {\n"
                qml += f"        idx: GamepadButton.{stick_mapping[layer.label]['axis']}\n"
                qml += f"        buttonidx: GamepadButton.{stick_mapping[layer.label]['button']}\n"
                qml += "        device: root.device\n"
                qml += f"        posX: {posX}\n"
                qml += f"        posY: {posY}\n"
                qml += f"        sizeWidth: {sizeWidth}\n"
                qml += f"        sizeHeight: {sizeHeight}\n"
                qml += "        controllerWidth: root.relativeWidth\n"
                qml += "        controllerHeight: root.relativeHeight\n"
                qml += f"        image: \"./svg/{prefix}_{layer.label}.svg\"\n"
                qml += "    }\n\n"

                self.export_svg_file(layerid, filename, filepath)

            elif layer.label and layer.label == "Controller":
                self.export_svg_file(layerid, filename, filepath)


        qml += "}\n"

        gamepadtypes_path = os.path.join(path, "gamepadtypes")
        os.makedirs(gamepadtypes_path, exist_ok=True)

        with open(os.path.join(gamepadtypes_path, f"{prefix}Gamepad.qml"), "w") as f:
            f.write(qml)

        self.msg("Export complete")

    def export_svg_file(self, layerid, filename, svgfilename):
        command = ["inkscape",
                   f"--export-filename={filename}",
                   "--export-overwrite",
                   "--export-area-snap",
                   f"--export-id={layerid}",
                   "--export-id-only",
                   "--export-plain-svg",
                   f"{svgfilename}"]

        try:
            with subprocess.Popen(command, stderr=subprocess.DEVNULL) as proc:
                proc.wait(timeout=300)
        except OSError:
            inkex.errormsg(f'Error while exporting file {command}.')
            exit()

if __name__ == '__main__':
    GamepadExportExtension().run()
