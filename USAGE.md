# Gamepad Design Tutorial

## Requirements

When designing a gamepad vector graphic, there is a few steps you need to follow:

* Make sure the layers are named properly. The names are also case-sensitive.
* The objects within the layers are only paths

### Button names

* D-pad Left
* D-pad Right
* D-pad Down
* D-Pad Up
* Back Button
    * - on Nintendo controllers
* Start Button
    * + on Nintendo controllers
* Guide Button
    * Home on Nintendo controllers
* Misc1 Button
    * Share button on certain Xbox controllers
* A Button
* B Button
* X Button
* Y Button
* Left Shoulder Button
* Right Shoulder Button

### Axis names

* Left Axis
* Right Axis

### Controller Image

The controller background layer must be called **Controller**.

## Exporting

To export the gamepad graphic to the Gamepad KCM, you must install the Inkscape extension from this repository (you can find the User Extensions directory for Inkscape in `Preferences > System`).

When the extension is correctly loaded, there is a new menu under `Extensions > Gamepad Export > Export Gamepad Symbolic`. Then point the two directory options to the Gamepad KCM source folders.
